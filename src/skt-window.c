/* skt-window.c
 *
 * Copyright 2020 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Socket"

#include "skt-config.h"

#include <glib/gi18n.h>

#include "skt-window.h"
#include "skt-application.h"


G_DEFINE_TYPE (SktWindow, skt_window, GTK_TYPE_APPLICATION_WINDOW)


typedef enum {
  SKT_MESSAGE_IN,
  SKT_MESSAGE_OUT,
  SKT_MESSAGE_ERROR,
  SKT_MESSAGE_STATUS,
} SktMessageType;


typedef enum {
  SKT_STATE_UNCONNECTED,
  SKT_STATE_CONNECTING,
  SKT_STATE_CONNECTED,
} SktState;


static void
skt_window_dispose (GObject *object)
{
  SktWindow *self = SKT_WINDOW (object);

  g_clear_object (&self->session);

  G_OBJECT_CLASS (skt_window_parent_class)->dispose (object);
}


static void
address_activate (GtkEntry *entry, SktWindow *self)
{
  g_action_group_activate_action (G_ACTION_GROUP (self), "connect", NULL);
}


static void
send_activate (GtkEntry *entry, SktWindow *self)
{
  g_action_group_activate_action (G_ACTION_GROUP (self), "send", NULL);
}


static void
skt_window_class_init (SktWindowClass *klass)
{
  GObjectClass   *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = skt_window_dispose;

  gtk_widget_class_set_template_from_resource (widget_class,
                                               RES_PATH "skt-window.ui");

  gtk_widget_class_bind_template_child (widget_class, SktWindow, stack);
  gtk_widget_class_bind_template_child (widget_class, SktWindow, address);
  gtk_widget_class_bind_template_child (widget_class, SktWindow, spinner);
  gtk_widget_class_bind_template_child (widget_class, SktWindow, title);
  gtk_widget_class_bind_template_child (widget_class, SktWindow, subtitle);
  gtk_widget_class_bind_template_child (widget_class, SktWindow, disconnect_btn);
  gtk_widget_class_bind_template_child (widget_class, SktWindow, send_bar);
  gtk_widget_class_bind_template_child (widget_class, SktWindow, entry);
  gtk_widget_class_bind_template_child (widget_class, SktWindow, buffer);

  gtk_widget_class_bind_template_callback (widget_class, address_activate);
  gtk_widget_class_bind_template_callback (widget_class, send_activate);
}


static void
about_activated (GSimpleAction *action,
                 GVariant      *parameter,
                 gpointer       data)
{
  SktWindow *self = data;
  const char *authors[] = { "Zander Brown <zbrown@gnome.org>", NULL };
  g_autofree char *copyright = NULL;

  copyright = g_strdup_printf (_("© %s Zander Brown"), "2020-2021");

  gtk_show_about_dialog (GTK_WINDOW (self),
                         "authors", authors,
                         // Translators: Credit yourself here
                         "translator-credits", _("translator-credits"),
                         "comments", _("Simple WebSocket Client"),
                         "copyright", copyright,
                         "license-type", GTK_LICENSE_GPL_3_0,
                         "logo-icon-name", "org.gnome.zbrown.Socket",
                         "version", PACKAGE_VERSION,
                         NULL);
}


static void
update_buffer (SktWindow *self, SktMessageType type, const char *message)
{
  GtkTextIter iter;

  gtk_text_buffer_get_end_iter (self->buffer, &iter);
  switch (type) {
    case SKT_MESSAGE_IN:
      gtk_text_buffer_insert_markup (self->buffer,
                                     &iter,
                                     "<b><span color=\"#2ec27e\">&lt;</span></b> ",
                                     -1);
      break;
    case SKT_MESSAGE_OUT:
      gtk_text_buffer_insert_markup (self->buffer,
                                     &iter,
                                     "<b><span color=\"#62a0ea\">&gt;</span></b> ",
                                     -1);
      break;
    case SKT_MESSAGE_ERROR:
      gtk_text_buffer_insert_markup (self->buffer,
                                     &iter,
                                     "<b><span color=\"#ed333b\">!</span></b> ",
                                     -1);
      break;
    case SKT_MESSAGE_STATUS:
      gtk_text_buffer_insert_markup (self->buffer,
                                     &iter,
                                     "<b><span color=\"#f6d32d\">#</span></b> ",
                                     -1);
      break;
    default:
    g_return_if_reached ();
  }

  gtk_text_buffer_get_end_iter (self->buffer, &iter);
  gtk_text_buffer_insert (self->buffer, &iter, message, -1);
  gtk_text_buffer_get_end_iter (self->buffer, &iter);
  gtk_text_buffer_insert (self->buffer, &iter, "\n", 1);
}


static void
title_set_state (SktWindow  *self,
                 SktState    state,
                 const char *title,
                 const char *subtitle)
{
  g_autofree char *window_title = NULL;

  gtk_spinner_stop (GTK_SPINNER (self->spinner));

  gtk_label_set_label (GTK_LABEL (self->title), title);
  gtk_label_set_label (GTK_LABEL (self->subtitle), subtitle);

  if (subtitle) {
    window_title = g_strdup_printf ("%s ― %s", title, subtitle);
    gtk_widget_show (self->subtitle);
  } else {
    window_title = g_strdup (title);
    gtk_widget_hide (self->subtitle);
  }

  gtk_window_set_title (GTK_WINDOW (self), window_title);

  switch (state) {
    case SKT_STATE_CONNECTING:
      gtk_spinner_start (GTK_SPINNER (self->spinner));
      gtk_stack_set_visible_child_name (GTK_STACK (self->stack), "working");
      gtk_widget_hide (self->disconnect_btn);
      break;
    case SKT_STATE_CONNECTED:
      gtk_stack_set_visible_child_name (GTK_STACK (self->stack), "label");
      gtk_widget_grab_focus (self->entry);
      gtk_widget_show (self->disconnect_btn);
      break;
    case SKT_STATE_UNCONNECTED:
    default:
      gtk_stack_set_visible_child_name (GTK_STACK (self->stack), "address");
      gtk_widget_grab_focus (self->address);
      gtk_widget_hide (self->disconnect_btn);
      break;
  }
}


static void
send_bar_set_visible (SktWindow *self, gboolean visible)
{
  gtk_revealer_set_reveal_child (GTK_REVEALER (self->send_bar), visible);

  if (visible) {
    gtk_widget_grab_focus (self->entry);
  } else {
    gtk_widget_grab_focus (self->address);
  }
}


static void
socket_closed (SoupWebsocketConnection *connection,
               gpointer                 data)
{
  SktWindow *self = data;

  update_buffer (self, SKT_MESSAGE_STATUS, "CLOSED");
  send_bar_set_visible (self, FALSE);
  title_set_state (self, SKT_STATE_UNCONNECTED, _("Socket"), NULL);
}


static void
socket_closing (SoupWebsocketConnection *connection,
                gpointer                 data)
{
  SktWindow *self = data;

  update_buffer (self, SKT_MESSAGE_STATUS, "CLOSING");
  send_bar_set_visible (self, FALSE);
  title_set_state (self, SKT_STATE_UNCONNECTED, _("Socket"), _("Closing…"));
}


static void
socket_error (SoupWebsocketConnection *connection,
              GError                  *error,
              gpointer                 data)
{
  SktWindow *self = data;

  update_buffer (self, SKT_MESSAGE_ERROR, error->message);
}


static void
socket_message (SoupWebsocketConnection *connection,
                int                      type,
                GBytes                  *message,
                gpointer                 data)
{
  SktWindow *self = data;

  update_buffer (self,
                 SKT_MESSAGE_IN,
                 (char *) g_bytes_get_data (message, NULL));
}


static void
got_connection (GObject *source, GAsyncResult *res, gpointer data)
{
  SoupSession *session = SOUP_SESSION (source);
  SktWindow *self = data;
  SoupURI *uri = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree char *subtitle = NULL;
  g_autofree char *full_uri = NULL;
  g_autofree char *msg = NULL;

  g_clear_object (&self->socket);
  self->socket = soup_session_websocket_connect_finish (session, res, &error);

  if (error) {
    update_buffer (self, SKT_MESSAGE_ERROR, error->message);
    title_set_state (self, SKT_STATE_UNCONNECTED, _("Socket"), NULL);

    return;
  }

  uri = soup_websocket_connection_get_uri (self->socket);
  full_uri = soup_uri_to_string (uri, FALSE);
  msg = g_strdup_printf ("CONNECTED “%s”", full_uri);

  update_buffer (self, SKT_MESSAGE_STATUS, msg);
  send_bar_set_visible (self, TRUE);

  subtitle = g_strdup_printf (":%i %s", soup_uri_get_port (uri), soup_uri_get_path (uri));
  title_set_state (self, SKT_STATE_CONNECTED, soup_uri_get_host (uri), subtitle);

  g_signal_connect (self->socket,
                    "closed",
                    G_CALLBACK (socket_closed),
                    self);
  g_signal_connect (self->socket,
                    "closing",
                    G_CALLBACK (socket_closing),
                    self);
  g_signal_connect (self->socket,
                    "error",
                    G_CALLBACK (socket_error),
                    self);
  g_signal_connect (self->socket,
                    "message",
                    G_CALLBACK (socket_message),
                    self);
}


static void
connect_activated (GSimpleAction *action,
                   GVariant      *parameter,
                   gpointer       data)
{
  SktWindow *self = data;
  g_autoptr (SoupMessage) msg = NULL;

  if (self->socket &&
      soup_websocket_connection_get_state (self->socket) == SOUP_WEBSOCKET_STATE_OPEN) {
    soup_websocket_connection_close (self->socket, 0, NULL);
  }

  msg = soup_message_new (SOUP_METHOD_GET,
                          gtk_editable_get_text (GTK_EDITABLE (self->address)));

  if (!msg) {
    update_buffer (self, SKT_MESSAGE_ERROR, _("Hmm"));
    title_set_state (self, SKT_STATE_UNCONNECTED, _("Socket"), NULL);

    return;
  }

  title_set_state (self, SKT_STATE_CONNECTING, _("Socket"), _("Connecting…"));

  soup_session_websocket_connect_async (self->session,
                                        msg,
                                        NULL,
                                        NULL,
                                        NULL,
                                        got_connection,
                                        self);
}


static void
disconnect_activated (GSimpleAction *action,
                      GVariant      *parameter,
                      gpointer       data)
{
  SktWindow *self = data;
  g_autoptr (SoupMessage) msg = NULL;

  if (!self->socket ||
      soup_websocket_connection_get_state (self->socket) != SOUP_WEBSOCKET_STATE_OPEN) {
    update_buffer (self, SKT_MESSAGE_ERROR, _("Not Connected"));

    return;
  }

  title_set_state (self, SKT_STATE_CONNECTING, _("Socket"), _("Disconnecting…"));
  soup_websocket_connection_close (self->socket, 0, NULL);
}


static void
send_activated (GSimpleAction *action,
                GVariant      *parameter,
                gpointer       data)
{
  SktWindow *self = data;
  const char *text = gtk_editable_get_text (GTK_EDITABLE (self->entry));

  if (!self->socket) {
    update_buffer (self, SKT_MESSAGE_ERROR, _("Not connected"));

    return;
  }

  update_buffer (self, SKT_MESSAGE_OUT, text);
  soup_websocket_connection_send_text (self->socket, text);

  gtk_editable_set_text (GTK_EDITABLE (self->entry), "");
  gtk_widget_grab_focus (GTK_WIDGET (self->entry));
}


static GActionEntry win_entries[] = {
  { "about", about_activated, NULL, NULL, NULL },
  { "connect", connect_activated, NULL, NULL, NULL },
  { "disconnect", disconnect_activated, NULL, NULL, NULL },
  { "send", send_activated, NULL, NULL, NULL },
};


static void
skt_window_init (SktWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  title_set_state (self, SKT_STATE_UNCONNECTED, _("Socket"), NULL);

  self->session = soup_session_new_with_options ("user-agent", "Socket " PACKAGE_VERSION,
                                                 "timeout", 15,
                                                 NULL);

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   win_entries,
                                   G_N_ELEMENTS (win_entries),
                                   self);
}
