/* skt-application.c
 *
 * Copyright 2020 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Socket"

#include "skt-config.h"

#include <glib/gi18n.h>

#include "skt-window.h"
#include "skt-application.h"


G_DEFINE_TYPE (SktApplication, skt_application, GTK_TYPE_APPLICATION)


static void
skt_application_activate (GApplication *app)
{
  GtkWindow *window;
  guint32 timestamp;

  timestamp = GDK_CURRENT_TIME;

  /* Get the current window or create one if necessary. */
  window = gtk_application_get_active_window (GTK_APPLICATION (app));
  if (window == NULL) {
    window = g_object_new (SKT_TYPE_WINDOW,
                           "application", app,
                           NULL);
  }

  gtk_window_present_with_time (window, timestamp);
}


static void
skt_application_startup (GApplication *app)
{
  GtkSettings    *gtk_settings;
  GtkCssProvider *provider;

  G_APPLICATION_CLASS (skt_application_parent_class)->startup (app);

  gtk_settings = gtk_settings_get_default ();

  g_object_set (gtk_settings, "gtk-application-prefer-dark-theme", TRUE, NULL);

  provider = gtk_css_provider_new ();
  gtk_css_provider_load_from_resource (provider, RES_PATH "styles.css");
  gtk_style_context_add_provider_for_display (gdk_display_get_default (),
                                              GTK_STYLE_PROVIDER (provider),
                                              GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
}


static void
skt_application_class_init (SktApplicationClass *klass)
{
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  app_class->activate = skt_application_activate;
  app_class->startup = skt_application_startup;
}


static void
skt_application_init (SktApplication *self)
{
}
