/* skt-window.h
 *
 * Copyright 2020 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include <libsoup/soup.h>

G_BEGIN_DECLS


#define SKT_TYPE_WINDOW (skt_window_get_type())

struct _SktWindow {
  GtkApplicationWindow     parent_instance;

  SoupSession             *session;
  SoupWebsocketConnection *socket;

  /* Template widgets */
  GtkWidget               *stack;
  GtkWidget               *address;
  GtkWidget               *spinner;
  GtkWidget               *title;
  GtkWidget               *subtitle;
  GtkWidget               *disconnect_btn;
  GtkWidget               *send_bar;
  GtkWidget               *entry;
  GtkTextBuffer           *buffer;
};


G_DECLARE_FINAL_TYPE (SktWindow, skt_window, SKT, WINDOW, GtkApplicationWindow)


G_END_DECLS
